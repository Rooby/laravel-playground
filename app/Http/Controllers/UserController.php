<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use Illuminate\Http\Request;
use App\Http\Requests\ListUsers;
use App\Http\Requests\UpdateUser;

class UserController extends Controller
{
    protected function register(Request $request)
    {
        $user = UserModel::register($request->all());
        return response($user, 200);
    }

    public function checkEmail(Request $request)
    {
        $exists = UserModel::whereEmail($request->email)->exists();
        return response(["exists" => $exists], 200);
    }

    public function checkUserEmail(Request $request, $id)
    {
        $user = UserModel::findOrFail($id);
        if ($user->email == $request->email) {
            return response(["error" => false], 200);
        } else {
            $exists = UserModel::whereEmail($request->email)->exists();
            return response(["error" => $exists], 200);
        }
    }

    public function list(ListUsers $request)
    {
        $users = UserModel::list($request->limit, $request->offset, $request->filter, $request->columns, $request->sortColumn, $request->sortDirection);
        $filtered = UserModel::countFiltered($request->filter, $request->columns);

        $response = [
            "data" => $users,
            "filtered" => $filtered
        ];
        return response($response, 200);
    }

    public function delete($id)
    {
        $user = UserModel::findOrFail($id)->delete();
        $response = [
            "message" => "User with ID: " . $id . " was deleted."
        ];
        return response($response, 200);
    }

    public function update(UpdateUser $request, $id)
    {
        $user = UserModel::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();

        $response = [
            "user" => $user
        ];

        return response($response, 200);
    }
}
