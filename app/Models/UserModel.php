<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function register($data)
    {
        $user = new UserModel();
        $user->name = $data["name"];
        $user->email = $data["email"];
        $user->password = bcrypt($data["password"]);
        $user->save();
        return $user;
    }

    public static function countFiltered($filter, $columns)
    {
        $filtered = UserModel::where(function($query) use ($filter, $columns) {
            foreach ($columns as $column) {
                $query->orWhere($column, "LIKE", "%" . $filter . "%");
            }
        })->count();

        return $filtered;
    }

    public static function list(int $limit, int $offset, $filter, $columns, $sortColumn, $sortDirection)
    {
        $users = self::where(function($query) use ($filter, $columns){
            foreach ($columns as $column) {
                $query->orWhere($column, "LIKE", "%" . $filter . "%");
            }
        })->limit($limit)->offset($offset)
        ->when($sortColumn && $sortDirection, function($query) use ($sortColumn, $sortDirection) {
            $query->orderBy($sortColumn, $sortDirection);
        })->get();
        return $users;
    }
}
