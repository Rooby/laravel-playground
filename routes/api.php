<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(["cors", "auth:api"])->group(function() {
    Route::prefix("user")->group(function() {
        Route::post("check-email/{id}", "UserController@checkUserEmail");
        Route::post("list", "UserController@list");

        Route::delete("{id}", "UserController@delete");

        Route::put("{id}", "UserController@update");
    });
});

Route::middleware(["cors"])->group(function() {
    Route::prefix("user")->group(function() {
        Route::post("register", "UserController@register");
        Route::post("check-email", "UserController@checkEmail");
    });
});
